package com.atlassian.jira.plugins.workflow.sharing.file;

import java.io.File;
import java.io.IOException;

public interface FileManager
{
    File createExportedWorkflowFile() throws IOException, CanNotCreateFileException;

    File getExportedWorkflowFile(String filename) throws IOException;

    void clearOlderThan(long ageInMs) throws IOException;

    void addExportedFileNameMapping(String actualName, String nameToDisplay);

    String getExportFileNameToDisplay(String actualFilename);

    void clearExportFileNameMapping(String actualName);

    void delete(File file);

    void deleteAllWorkflowFiles() throws IOException;
}
