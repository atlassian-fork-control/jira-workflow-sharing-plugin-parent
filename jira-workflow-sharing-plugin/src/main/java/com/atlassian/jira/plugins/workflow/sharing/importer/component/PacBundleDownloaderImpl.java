package com.atlassian.jira.plugins.workflow.sharing.importer.component;

import com.atlassian.jira.plugins.workflow.sharing.importer.ImporterConstants;
import com.atlassian.jira.plugins.workflow.sharing.servlet.ValidationException;
import com.atlassian.jira.plugins.workflow.sharing.util.HttpResult;
import com.atlassian.jira.plugins.workflow.sharing.util.HttpUtils;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.sal.api.message.I18nResolver;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URISyntaxException;

@ExportAsService
@Component
public class PacBundleDownloaderImpl implements PacBundleDownloader
{
    private final I18nResolver i18n;
    private final WorkflowBundle.Factory bundleFactory;

    @Autowired
    public PacBundleDownloaderImpl(I18nResolver i18n, WorkflowBundle.Factory bundleFactory)
    {
        this.i18n = i18n;
        this.bundleFactory = bundleFactory;
    }

    @Override
    public WorkflowBundle downloadBundle(final String downloadUri) throws IOException, URISyntaxException, ValidationException
    {
        URI uri = new URI(downloadUri);

        HttpGet method = new HttpGet(uri);

        HttpResult result = null;
        InputStream is = null;
        try
        {
            result = HttpUtils.execute(method);
            HttpResponse response = result.getResponse();

            final long responseContentLength = response.getEntity().getContentLength();
            if (responseContentLength > ImporterConstants.MAX_STREAM)
            {
                throw new ValidationException(i18n.getText("wfshare.exception.workflow.too.big"));
            }

            int status = response.getStatusLine().getStatusCode();

            if (HttpStatus.SC_OK != status)
            {
                throw new ValidationException(i18n.getText("wfshare.exception.download.error", status));
            }

            is = response.getEntity().getContent();
            return bundleFactory.bundle(is, WorkflowBundle.BundleSource.MPAC);
        }
        finally
        {
            IOUtils.closeQuietly(is);

            if (result != null)
            {
                result.close();
            }
        }
    }
}
