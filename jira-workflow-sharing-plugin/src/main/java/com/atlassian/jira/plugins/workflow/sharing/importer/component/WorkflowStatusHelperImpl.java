package com.atlassian.jira.plugins.workflow.sharing.importer.component;

import com.atlassian.jira.bc.ServiceOutcome;
import com.atlassian.jira.bc.ServiceResult;
import com.atlassian.jira.bc.config.StatusService;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.status.SimpleStatus;
import com.atlassian.jira.issue.status.SimpleStatusImpl;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.issue.status.category.StatusCategory;
import com.atlassian.jira.issue.status.category.StatusCategoryImpl;
import com.atlassian.jira.plugins.workflow.sharing.importer.StatusMapping;
import com.atlassian.jira.plugins.workflow.sharing.importer.ValidNameGenerator;
import com.atlassian.jira.plugins.workflow.sharing.model.StatusInfo;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowUtil;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.sal.api.message.I18nResolver;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.google.common.collect.Maps;
import com.opensymphony.workflow.FactoryException;
import com.opensymphony.workflow.loader.StepDescriptor;
import com.opensymphony.workflow.loader.WorkflowDescriptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.List;
import java.util.Map;

@ExportAsService
@Component
public class WorkflowStatusHelperImpl implements WorkflowStatusHelper
{
    private final ConstantsManager constantsManager;
    private final I18nResolver i18n;
    private final ValidNameGenerator validNameGenerator;
    private final JiraAuthenticationContext authenticationContext;
    private final StatusService statusService;

    @Autowired
    public WorkflowStatusHelperImpl(ConstantsManager constantsManager, I18nResolver i18n, ValidNameGenerator validNameGenerator, JiraAuthenticationContext authenticationContext, StatusService statusService)
    {
        this.constantsManager = constantsManager; 
        this.i18n = i18n;
        this.validNameGenerator = validNameGenerator;
        this.authenticationContext = authenticationContext;
        this.statusService = statusService;
    }

    @Override
    public Map<String,StatusMapping> getStatusHolders(String xml)
    {
        return getStatusHolders(xml, null);
    }

    @Override
    /**
     * Examine the workflow we're importing to determine what {@link Status} objects to associate with the workflow.
     * Depending on the user's choice, they may need to be created, or may simply re-use existing system Statuses.
     *
     * @param xml XML description of the workflow to import.
     * @param statusInfo a list of {@link StatusInfo} objects exported from the original system, linked to the workflow to import.
     * @return a mapping of IDs for Workflow Step {@link Status}es to the data required to reconstitute them.
     */
    public Map<String, StatusMapping> getStatusHolders(String xml, List<StatusInfo> statusInfo)
    {
        final Collection<Status> jiraStatuses = constantsManager.getStatusObjects();
        final Map<String,StatusInfo> statusInfoMap = Maps.newHashMap();
        if (null != statusInfo)
        {
            for (StatusInfo info : statusInfo)
            {
                statusInfoMap.put(info.getOriginalId(), info);
            }
        }

        Map<String,StatusMapping> holders = Maps.newHashMap();

        final WorkflowDescriptor descriptor = getWorkflowDescriptor(xml);
        final List<StepDescriptor> steps = descriptor.getSteps();
        final Long defaultStatusCategoryId = StatusCategoryImpl.getDefault().getId();

        for (StepDescriptor step : steps)
        {
            if ((step.getMetaAttributes() != null) && step.getMetaAttributes().containsKey(JiraWorkflow.STEP_STATUS_KEY))
            {
                // Let's assign data for a Status based on whether we should try mapping to an existing status or just create a new one.

                final String statusId = (String) step.getMetaAttributes().get(JiraWorkflow.STEP_STATUS_KEY);
                final Status found = Iterables.find(jiraStatuses, new StatusNamePredicate(step.getName()), null);
                final StatusMapping mapping;


                if (found != null)
                {
                    // We found an existing Status in the new system. Let's reference it as the Status for this workflow Step.
                    final Long statusCategoryId = (null == found.getStatusCategory()) ? defaultStatusCategoryId : found.getStatusCategory().getId();
                    mapping = new StatusMapping(statusId, step.getName(), found.getId(), getValidStatusName(found.getName()), statusCategoryId);
                }
                else if (statusInfoMap.containsKey(statusId))
                {
                    // The system the workflow was exported from has info for this step's associated Status, so let's use that data to create a new Status.
                    final StatusInfo info = statusInfoMap.get(statusId);
                    mapping = new StatusMapping(info.getOriginalId(), info.getName(), "-1", getValidStatusName(info.getName()), info.getStatusCategoryId());
                }
                else
                {
                    // There's no data for this new Status apart from the Step's name in the workflow. We'll use that and default values for everything else.
                    mapping = new StatusMapping(statusId, step.getName(), "-1", getValidStatusName(step.getName()), defaultStatusCategoryId);
                }
                holders.put(statusId, mapping);
            }
        }

        return holders;
    }

    private WorkflowDescriptor getWorkflowDescriptor(String xml)
    {
        WorkflowDescriptor descriptor;
        try
        {
            descriptor = WorkflowUtil.convertXMLtoWorkflowDescriptor(xml);
        }
        catch (FactoryException e)
        {
            throw new RuntimeException(e);
        }
        return descriptor;
    }

    @Override
    public List<Status> getJiraStatuses()
    {
        return ImmutableList.copyOf(constantsManager.getStatusObjects());
    }

    @Override
    public void removeStatus(String id) throws Exception
    {
        ServiceResult serviceResult = statusService.removeStatus(authenticationContext.getUser(), constantsManager.getStatusObject(id));

        if (!serviceResult.isValid())
        {
            throw new Exception();
        }
    }

    @Override
    public Status createStatus(String newName, String newStatusDefaultIcon) throws Exception
    {
        return createStatus(newName, newStatusDefaultIcon, StatusCategoryImpl.getDefault().getId());
    }

    @Override
    public Status createStatus(StatusMapping mapping, String newStatusDefaultIcon) throws Exception
    {
        return createStatus(mapping.getNewName(), newStatusDefaultIcon, mapping.getStatusCategoryId());
    }

    private Status createStatus(String newName, String newStatusDefaultIcon, Long newStatusCategoryId) throws Exception
    {
        final String validName = getValidStatusName(newName);
        final StatusCategory category = getCategory(newStatusCategoryId);

        ServiceOutcome<Status> serviceOutcome = statusService.createStatus(authenticationContext.getUser(), validName, "", newStatusDefaultIcon, category);

        if (!serviceOutcome.isValid())
        {
            throw new Exception();
        }

        return serviceOutcome.getReturnedValue();
    }

    @Override
    public String getValidStatusName(String name)
    {
        return validNameGenerator.getValidName(name, 60, this::statusNameExistis);
    }

    @Override
    public String getNameForStatusId(String id)
    {
        String name = i18n.getText("wfshare.exception.status.not.found");
        
        Status status = constantsManager.getStatusObject(id);
        if(null != status)
        {
            name = status.getName();
        }
        
        return name;
    }

    @Override
    public SimpleStatus getSimpleStatusFromMapping(StatusMapping statusMapping)
    {
        return new SimpleStatusImpl(
                "",
                statusMapping.getNewName(),
                "",
                getCategory(statusMapping.getStatusCategoryId()),
                ""
        );
    }

    private StatusCategory getCategory(Long categoryId)
    {
        return (null == categoryId) ? StatusCategoryImpl.getDefault() : StatusCategoryImpl.findById(categoryId);
    }

    private Boolean statusNameExistis(String statusName) {
        return constantsManager.getStatuses()
                .stream()
                .map(Status::getName)
                .anyMatch(statusName::equalsIgnoreCase);
    }

    private class StatusNamePredicate implements Predicate<Status>
    {
        private final String name;

        private StatusNamePredicate(String name)
        {
            this.name = name;
        }

        @Override
        public boolean apply(Status status)
        {
            return name.equalsIgnoreCase(status.getName());
        }
    }
    
}
