package com.atlassian.jira.plugins.workflow.sharing.importer.component;

import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.issue.fields.screen.FieldScreenImpl;
import com.atlassian.jira.issue.fields.screen.FieldScreenManager;
import com.atlassian.jira.issue.fields.screen.FieldScreenTab;
import com.atlassian.jira.plugins.workflow.sharing.WorkflowCustomFieldsHelper;
import com.atlassian.jira.plugins.workflow.sharing.importer.ValidNameGenerator;
import com.atlassian.jira.plugins.workflow.sharing.model.ScreenInfo;
import com.atlassian.jira.plugins.workflow.sharing.model.ScreenItemInfo;
import com.atlassian.jira.plugins.workflow.sharing.model.ScreenTabInfo;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.google.common.collect.Ordering;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.List;
import java.util.Map;

@ExportAsService
@Component
public class ScreenCreatorImpl implements ScreenCreator
{
    private final FieldScreenManager fieldScreenManager;
    private final ValidNameGenerator validNameGenerator;

    @Autowired
    public ScreenCreatorImpl(FieldScreenManager fieldScreenManager, ValidNameGenerator validNameGenerator)
    {
        this.fieldScreenManager = fieldScreenManager;
        this.validNameGenerator = validNameGenerator;
    }

    @Override
    public void removeScreen(Long screenId)
    {
        fieldScreenManager.removeFieldScreen(screenId);
    }

    @Override
    public FieldScreen createScreen(ScreenInfo screenInfo)
    {
        String validName = getValidName(screenInfo.getName());
        FieldScreen screen = new FieldScreenImpl(fieldScreenManager);

        screen.setName(validName);
        screen.setDescription(screenInfo.getDescription());
        screen.store();

        return screen;
    }

    @Override
    public void addScreenTabs(FieldScreen screen, ScreenInfo screenInfo, Map<String, String> createdFieldsMapping)
    {
        for (ScreenTabInfo tabInfo : screenInfo.getTabs())
        {
            FieldScreenTab tab = screen.addTab(tabInfo.getName());
            tab.setPosition(tabInfo.getPosition());

            List<ScreenItemInfo> sortedItems = Ordering.from(new Comparator<ScreenItemInfo>() {
                @Override
                public int compare(ScreenItemInfo o1, ScreenItemInfo o2)
                {
                    return o1.getPosition().compareTo(o2.getPosition());
                }
            }).sortedCopy(tabInfo.getItems());

            int position = 0;
            for (ScreenItemInfo itemInfo : sortedItems)
            {
                if (!itemInfo.getFieldId().startsWith(WorkflowCustomFieldsHelper.CUSTOM_FIELD_PREFIX) || createdFieldsMapping.containsKey(itemInfo.getFieldId()))
                {
                    String newId = itemInfo.getFieldId();
                    
                    if (itemInfo.getFieldId().startsWith(WorkflowCustomFieldsHelper.CUSTOM_FIELD_PREFIX))
                    {
                        newId = createdFieldsMapping.get(itemInfo.getFieldId());
                    }
                    
                    tab.addFieldScreenLayoutItem(newId, position);
                    position++;
                }
            }
        }
    }

    @Override
    public String getValidName(String originalName)
    {
        return validNameGenerator.getValidName(originalName, 255, fieldScreenManager::screenNameExists);
    }

}
