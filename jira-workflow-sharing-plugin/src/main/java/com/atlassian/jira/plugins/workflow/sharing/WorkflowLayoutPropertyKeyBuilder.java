package com.atlassian.jira.plugins.workflow.sharing;

import static org.apache.commons.codec.digest.DigestUtils.md5Hex;

/**
 * This is copied from jira-workflow-designer code since it's not exported for use by other bundles
 */
public abstract class WorkflowLayoutPropertyKeyBuilder
{
    private String workflowName;

    private WorkflowState workflowState;

    private boolean version2;

    protected WorkflowState getWorkflowState()
    {
        return workflowState;
    }

    protected String getWorkflowName()
    {
        return workflowName;
    }

    public static WorkflowLayoutPropertyKeyBuilder newBuilder()
    {
        return new WorkflowLayoutPropertyKeyBuilder.MD5();
    }

    public WorkflowLayoutPropertyKeyBuilder setWorkflowName(final String workflowName)
    {
        this.workflowName = workflowName;
        return this;
    }

    public WorkflowLayoutPropertyKeyBuilder setWorkflowState(final WorkflowState workflowState)
    {
        this.workflowState = workflowState;
        return this;
    }

    public WorkflowLayoutPropertyKeyBuilder setVersion2(boolean version2)
    {
        this.version2 = version2;
        return this;
    }

    public boolean isVersion2()
    {
        return version2;
    }

    public abstract String build();

    private static class MD5 extends WorkflowLayoutPropertyKeyBuilder
    {
        @Override
        public String build()
        {
            StringBuilder prefix = new StringBuilder(getWorkflowState().keyPrefix());
            if (isVersion2())
            {
                prefix.append(".v5");
            }
            return prefix.append(":").append(md5Hex(getWorkflowName())).toString();
        }
    }

    public enum WorkflowState
    {
        LIVE
                {
                    @Override
                    String keyPrefix()
                    {
                        return "jira.workflow.layout";
                    }
                },
        DRAFT
                {
                    @Override
                    String keyPrefix()
                    {
                        return "jira.workflow.draft.layout";
                    }
                };

        abstract String keyPrefix();
    }
}
