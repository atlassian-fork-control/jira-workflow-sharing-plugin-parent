package com.atlassian.jira.plugins.workflow.sharing.exporter;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import com.atlassian.jira.plugin.workflow.AbstractWorkflowModuleDescriptor;
import com.atlassian.jira.plugins.workflow.sharing.ModuleDescriptorLocator;
import com.atlassian.jira.plugins.workflow.sharing.RemovedItems;
import com.atlassian.jira.plugins.workflow.sharing.WorkflowExtensionsHelper;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.plugin.ModuleDescriptor;

import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.opensymphony.workflow.loader.ActionDescriptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import static com.atlassian.jira.plugin.workflow.JiraWorkflowPluginConstants.MODULE_NAME_WORKFLOW_CONDITION;
import static com.atlassian.jira.plugin.workflow.JiraWorkflowPluginConstants.MODULE_NAME_WORKFLOW_FUNCTION;
import static com.atlassian.jira.plugin.workflow.JiraWorkflowPluginConstants.MODULE_NAME_WORKFLOW_VALIDATOR;

@ExportAsService
@Component
public class WorkflowExportNotesProviderImpl implements WorkflowExportNotesProvider
{
    private final WorkflowExtensionsHelper workflowExtensionsHelper;
    private final ModuleDescriptorLocator moduleDescriptorLocator;
    private final JiraAuthenticationContext jiraAuthenticationContext;

    @Autowired
    public WorkflowExportNotesProviderImpl(WorkflowExtensionsHelper workflowExtensionsHelper, ModuleDescriptorLocator moduleDescriptorLocator, JiraAuthenticationContext jiraAuthenticationContext)
    {
        this.workflowExtensionsHelper = workflowExtensionsHelper;
        this.moduleDescriptorLocator = moduleDescriptorLocator;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    @Override
    public String createNotes(JiraWorkflow workflow)
    {
        RemovedItems removedItems = workflowExtensionsHelper.getRemovedItems(workflow, true);

        if (removedItems.isEmpty())
        {
            return "";
        }

        NotesData notesData = new NotesData();

        collectData(removedItems, notesData);

        return generateNotes(notesData);
    }

    private void collectData(RemovedItems removedItems, NotesData notesData)
    {
        for (RemovedItems.ActionRemovedItems actionRemovedItems : removedItems.getActionRemovedItems())
        {
            if (!actionRemovedItems.isEmpty())
            {
                ActionDescriptor action = actionRemovedItems.getAction();

                NotesData.ActionData actionData = new NotesData.ActionData(action.getName());

                Collection<String> allPluginNames = notesData.getAllPluginNames();
                collectCategoryData(actionRemovedItems.getConditions(), actionData.getPluginConditions(), actionData.getSystemConditions(), MODULE_NAME_WORKFLOW_CONDITION, allPluginNames);
                collectCategoryData(actionRemovedItems.getValidators(), actionData.getPluginValidators(), actionData.getSystemValidators(), MODULE_NAME_WORKFLOW_VALIDATOR, allPluginNames);
                collectCategoryData(actionRemovedItems.getFunctions(), actionData.getPluginFunctions(), actionData.getSystemFunctions(), MODULE_NAME_WORKFLOW_FUNCTION, allPluginNames);
                collectCategoryData(actionRemovedItems.getCustomFields(), actionData.getPluginCustomFields(), null, null, allPluginNames);

                actionData.setReplacedEventId(actionRemovedItems.isReplacedEventId());

                notesData.add(actionData);
            }
        }
    }

    private String generateNotes(NotesData notesData)
    {
        StringWriter sw = new StringWriter();
        PrintWriter writer = new PrintWriter(sw);

        if (!notesData.isEmpty())
        {
            writer.println(getText("wfshare.export.notes.removed.message"));

            printList(notesData.getAllPluginNames(), writer);

            for (NotesData.ActionData actionData : notesData.getActions())
            {
                if (!actionData.isEmpty())
                {
                    String actionName = actionData.getTransitionName();

                    printHeading(writer, "wfshare.export.notes.transition", actionName);

                    writeCategory(actionData.getPluginConditions(), "wfshare.export.notes.plugin.conditions", writer);
                    writeCategory(actionData.getSystemConditions(), "wfshare.export.notes.system.conditions", writer);

                    writeCategory(actionData.getPluginValidators(), "wfshare.export.notes.plugin.validators", writer);
                    writeCategory(actionData.getSystemValidators(),"wfshare.export.notes.system.validators", writer);

                    writeCategory(actionData.getPluginFunctions(), "wfshare.export.notes.plugin.functions", writer);
                    writeCategory(actionData.getSystemFunctions(), "wfshare.export.notes.system.functions", writer);
                    if (actionData.isReplacedEventId())
                    {
                        writer.println();
                        writer.println(getText("wfshare.export.notes.custom.event.replaced"));
                    }

                    writeCategory(actionData.getPluginCustomFields(), "wfshare.export.notes.plugin.custom.fields", writer);
                }
            }
        }

        return sw.toString();
    }

    private void collectCategoryData(Collection<String> classNames, Collection<String> pluginNotes, Collection<String> systemNotes, String systemPluginType, Collection<String> allPluginNames)
    {
        if (!classNames.isEmpty())
        {
            Set<String> systemClasses = new HashSet<String>();

            for (String className : classNames)
            {
                Collection<ModuleDescriptor> descriptors = moduleDescriptorLocator.getEnabledModuleDescriptorsByModuleClassname(className);

                if (descriptors != null && !descriptors.isEmpty())
                {
                    ModuleDescriptor descriptor = descriptors.iterator().next();

                    String pluginName = descriptor.getPlugin().getName();
                    String note = descriptor.getName() + " (" + pluginName + ")";

                    pluginNotes.add(note);
                    allPluginNames.add(pluginName);
                }
                else if (systemPluginType != null)
                {
                    systemClasses.add(className);
                }
            }

            if (!systemClasses.isEmpty())
            {
                Collection<ModuleDescriptor> descriptors = moduleDescriptorLocator.getEnabledModuleDescriptorsByModuleType(systemPluginType);

                if (descriptors != null && !descriptors.isEmpty())
                {
                    for (String systemClass : systemClasses)
                    {
                        for (ModuleDescriptor descriptor : descriptors)
                        {
                            if (descriptor instanceof AbstractWorkflowModuleDescriptor)
                            {
                                AbstractWorkflowModuleDescriptor workflowModuleDescriptor = (AbstractWorkflowModuleDescriptor) descriptor;
                                if (workflowModuleDescriptor.getImplementationClass().getName().equals(systemClass))
                                {
                                    String note = descriptor.getName();

                                    systemNotes.add(note);

                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    private void writeCategory(Collection<String> notes, String heading, PrintWriter writer)
    {
        if (!notes.isEmpty())
        {
            printHeading(writer, heading);

            printList(notes, writer);
        }
    }

    private void printList(Collection<String> notes, PrintWriter writer)
    {
        for (String note : notes)
        {
            writer.println(" - " + note);
        }
    }

    private void printHeading(PrintWriter writer, String message, String... parameters)
    {
        writer.println();
        writer.println(getText(message, (Object[])parameters));
    }

    private String getText(String key, Object... parameters)
    {
        return jiraAuthenticationContext.getI18nHelper().getText(key, parameters);
    }


    private static class NotesData
    {
        private final Collection<ActionData> actions = Lists.newArrayList();
        private final Collection<String> allPluginNames = Sets.newTreeSet();

        void add(ActionData actionData)
        {
            actions.add(actionData);
        }

        Collection<ActionData> getActions()
        {
            return actions;
        }

        public Collection<String> getAllPluginNames()
        {
            return allPluginNames;
        }

        public boolean isEmpty()
        {
            for (ActionData actionData : actions)
            {
                if (!actionData.isEmpty())
                {
                    return false;
                }
            }
            return true;
        }

        private static class ActionData
        {
            private final String transitionName;

            private final Collection<String> pluginConditions = Sets.newTreeSet();
            private final Collection<String> pluginValidators = Sets.newTreeSet();
            private final Collection<String> pluginFunctions = Sets.newTreeSet();
            private final Collection<String> pluginCustomFields = Sets.newTreeSet();
            private final Collection<String> systemConditions = Sets.newTreeSet();
            private final Collection<String> systemValidators = Sets.newTreeSet();
            private final Collection<String> systemFunctions = Sets.newTreeSet();

            private boolean replacedEventId;

            private ActionData(String transitionName)
            {
                this.transitionName = transitionName;
            }

            public String getTransitionName()
            {
                return transitionName;
            }

            public Collection<String> getPluginConditions()
            {
                return pluginConditions;
            }

            public Collection<String> getPluginValidators()
            {
                return pluginValidators;
            }

            public Collection<String> getPluginFunctions()
            {
                return pluginFunctions;
            }

            public Collection<String> getPluginCustomFields()
            {
                return pluginCustomFields;
            }

            public Collection<String> getSystemConditions()
            {
                return systemConditions;
            }

            public Collection<String> getSystemValidators()
            {
                return systemValidators;
            }

            public Collection<String> getSystemFunctions()
            {
                return systemFunctions;
            }

            public void setReplacedEventId(boolean replacedEventId)
            {
                this.replacedEventId = replacedEventId;
            }

            public boolean isReplacedEventId()
            {
                return replacedEventId;
            }

            public boolean isEmpty()
            {
                return pluginConditions.isEmpty() && pluginValidators.isEmpty() && pluginFunctions.isEmpty() && pluginCustomFields.isEmpty()
                         && systemConditions.isEmpty() && systemValidators.isEmpty() && systemFunctions.isEmpty() && !replacedEventId;
            }
        }
    }
}
