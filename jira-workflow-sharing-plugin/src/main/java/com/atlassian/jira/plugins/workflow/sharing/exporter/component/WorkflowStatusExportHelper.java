package com.atlassian.jira.plugins.workflow.sharing.exporter.component;

import com.atlassian.jira.issue.status.Status;

import java.io.IOException;
import java.util.List;

public interface WorkflowStatusExportHelper
{
    String getStatusesJson(List<Status> statuses) throws IOException;
}
