package com.atlassian.jira.plugins.workflow.sharing.importer;

import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.function.Function;

import static org.apache.commons.lang.StringUtils.abbreviate;

@Component
public class ValidNameGenerator
{
    private final JiraAuthenticationContext jiraAuthenticationContext;

    @Autowired
    public ValidNameGenerator(JiraAuthenticationContext jiraAuthenticationContext)
    {
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    public String getValidName(String originalName, int maxLength, Function<String, Boolean> nameAlreadyExists)
    {
        String name = abbreviate(originalName, maxLength);

        if (!nameAlreadyExists.apply(name))
        {
            return name;
        }

        I18nHelper i18nHelper = jiraAuthenticationContext.getI18nHelper();

        int j = 2;
        while (true)
        {
            String prefix = i18nHelper.getText("wfshare.import.copy.prefix", String.valueOf(j++));
            int prefixLength = prefix.length();

            int abbreviateTo = maxLength - prefixLength;
            String abbreviatedName = abbreviate(originalName, abbreviateTo);

            name = i18nHelper.getText("wfshare.import.copy", abbreviatedName, prefix);

            if (!nameAlreadyExists.apply(name))
            {
                return name;
            }
        }
    }
}
