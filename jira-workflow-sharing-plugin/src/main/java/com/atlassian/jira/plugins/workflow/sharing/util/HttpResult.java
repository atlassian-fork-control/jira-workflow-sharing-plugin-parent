package com.atlassian.jira.plugins.workflow.sharing.util;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.conn.ClientConnectionManager;

public class HttpResult
{
    private final HttpResponse response;
    private final ClientConnectionManager connectionManager;
    private final HttpRequestBase method;

    HttpResult(HttpResponse response, ClientConnectionManager connectionManager, HttpRequestBase method)
    {
        this.response = response;
        this.connectionManager = connectionManager;
        this.method = method;
    }

    public HttpResponse getResponse()
    {
        return response;
    }

    public void close()
    {
        method.releaseConnection();
        connectionManager.shutdown();
    }
}
