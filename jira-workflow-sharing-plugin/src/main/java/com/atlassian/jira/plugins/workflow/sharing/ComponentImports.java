package com.atlassian.jira.plugins.workflow.sharing;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.bc.config.StatusService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.templaterenderer.TemplateRenderer;
import org.springframework.stereotype.Component;

@Component
public class ComponentImports {

    @ComponentImport
    EventPublisher eventPublisher;
    @ComponentImport
    com.sysbliss.jira.plugins.workflow.util.WorkflowDesignerPropertySet wdPropertySet;
    @ComponentImport
    com.atlassian.sal.api.user.UserManager userManager;
    @ComponentImport
    com.atlassian.sal.api.auth.LoginUriProvider loginUriProvider;
    @ComponentImport
    com.atlassian.sal.api.ApplicationProperties applicationProperties;
    @ComponentImport
    com.atlassian.jira.template.soy.SoyTemplateRendererProvider soyProvider;
    @ComponentImport
    com.atlassian.sal.api.message.I18nResolver i18nResolver;
    @ComponentImport
    com.atlassian.sal.api.websudo.WebSudoManager salWebSudoManager;
    @ComponentImport
    com.atlassian.sal.api.net.RequestFactory salRequestFactory;
    @ComponentImport
    com.atlassian.jira.config.util.JiraHome jiraHome;
    @ComponentImport
    com.atlassian.jira.config.ConstantsManager constantsManager;
    @ComponentImport
    com.atlassian.jira.workflow.WorkflowManager workflowManager;
    @ComponentImport
    com.atlassian.plugin.PluginAccessor pluginAccessor;
    @ComponentImport
    com.atlassian.plugin.ModuleDescriptorFactory moduleDescriptorFactory;
    @ComponentImport
    com.atlassian.jira.security.JiraAuthenticationContext jiraAuthenticationContext;
    @ComponentImport
    com.atlassian.jira.issue.CustomFieldManager customFieldManager;
    @ComponentImport
    com.atlassian.jira.issue.fields.config.manager.FieldConfigSchemeManager fieldConfigSchemeManager;
    @ComponentImport
    com.atlassian.jira.issue.customfields.manager.OptionsManager optionsManager;
    @ComponentImport
    com.atlassian.jira.issue.fields.screen.FieldScreenManager fieldScreenManager;
    @ComponentImport
    com.atlassian.jira.user.util.UserUtil userUtil;
    @ComponentImport
    com.atlassian.jira.util.velocity.VelocityRequestContextFactory velocityRequestContextFactory;
    @ComponentImport
    com.atlassian.jira.bc.workflow.WorkflowService workflowService;
    @ComponentImport
    com.atlassian.jira.ofbiz.OfBizDelegator ofBizDelegator;
    @ComponentImport
    com.atlassian.sal.api.xsrf.XsrfTokenValidator xsrfTokenValidator;
    @ComponentImport
    com.atlassian.jira.security.xsrf.XsrfTokenGenerator xsrfTokenGenerator;
    @ComponentImport("jiraApplicationProperties")
    com.atlassian.jira.config.properties.ApplicationProperties jiraApplicationProperties;
    @ComponentImport
    com.atlassian.jira.project.ProjectManager projectManager;
    @ComponentImport
    com.atlassian.sal.api.message.HelpPathResolver helpPathResolver;
    @ComponentImport
    com.atlassian.sal.api.scheduling.PluginScheduler pluginScheduler;
    @ComponentImport
    StatusService statusService;
    @ComponentImport
    TemplateRenderer templateRenderer;
}
