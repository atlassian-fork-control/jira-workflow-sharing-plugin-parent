package com.atlassian.jira.plugins.workflow.sharing;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.customfields.CustomFieldSearcher;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.customfields.option.Options;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.plugins.workflow.sharing.model.CustomFieldInfo;
import com.atlassian.jira.plugins.workflow.sharing.model.OptionInfo;
import com.atlassian.plugin.ModuleDescriptor;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

@ExportAsService
@Component
public class WorkflowCustomFieldsHelperImpl implements WorkflowCustomFieldsHelper
{
    private final CustomFieldManager customFieldManager;
    private final ModuleDescriptorLocator moduleDescriptorLocator;
    private final WorkflowPluginHelper pluginHelper;

    @Autowired
    public WorkflowCustomFieldsHelperImpl(CustomFieldManager customFieldManager,
                                          ModuleDescriptorLocator moduleDescriptorLocator, WorkflowPluginHelper pluginHelper)
    {
        this.customFieldManager = customFieldManager;
        this.moduleDescriptorLocator = moduleDescriptorLocator;
        this.pluginHelper = pluginHelper;
    }

    @Override
    public String getCustomFieldsJson(Iterable<String> fields) throws IOException
    {
        Map<String, CustomFieldInfo> info = Maps.newHashMap();
        for (String fieldId : fields)
        {
            CustomField field = customFieldManager.getCustomFieldObject(fieldId);
            if (field != null && !info.containsKey(fieldId))
            {
                String typeName = field.getCustomFieldType().getClass().getName();
                if (!pluginHelper.isFromPlugin(typeName))
                {
                    info.put(fieldId, createCustomFieldInfo(field));
                }
            }
        }

        ObjectMapper mapper = new ObjectMapper();
        StringWriter sw = new StringWriter();
        mapper.writeValue(sw, info.values());
        return sw.toString();
    }

    private CustomFieldInfo createCustomFieldInfo(CustomField field)
    {
        String fieldId = field.getId();
        String fieldPluginKey = getPluginKeyProvidingField(fieldId);
        String fieldName = field.getName();
        String fieldDesc = field.getDescription();

        CustomFieldType cfType = field.getCustomFieldType();
        
        //using reflection to support both jira 4.4 & 5.0
        //in 4.4 CustomFieldTypeModuleDescriptor is a class but in 5.0 it's an interface

        String ctkey = cfType.getDescriptor().getCompleteKey();

        CustomFieldSearcher cfSearcher = field.getCustomFieldSearcher();
        String searcherKey = "";
        if(null != cfSearcher)
        {
            searcherKey = cfSearcher.getDescriptor().getCompleteKey();
        }

        ConfigurationSchemeInfo schemeInfo = getConfigurationSchemeInfo(field);

        return new CustomFieldInfo(
                fieldId
                ,fieldName
                ,fieldDesc
                ,ctkey
                ,searcherKey
                ,schemeInfo.getOptionInfo()
                ,fieldPluginKey
        );
    }

    public ConfigurationSchemeInfo getConfigurationSchemeInfo(CustomField field)
    {
        List<OptionInfo> optionInfo;

        List<FieldConfigScheme> configurationSchemes = field.getConfigurationSchemes();
        FieldConfigScheme globalFieldConfigScheme = null;

        for (FieldConfigScheme fieldConfigScheme : configurationSchemes)
        {
            if (fieldConfigScheme.isGlobal())
            {
                globalFieldConfigScheme = fieldConfigScheme;

                break;
            }
        }

        if (globalFieldConfigScheme != null)
        {
            FieldConfig fieldConfig = globalFieldConfigScheme.getOneAndOnlyConfig();

            Options options = field.getOptions(null, fieldConfig, null);
            optionInfo = getOptionInfo(options);
        }
        else
        {
            optionInfo = Lists.newArrayList();
        }

        return new ConfigurationSchemeInfo(optionInfo);
    }

    public static class ConfigurationSchemeInfo
    {
        private final List<OptionInfo> optionInfo;

        ConfigurationSchemeInfo(List<OptionInfo> optionInfo)
        {
            this.optionInfo = optionInfo;
        }

        public List<OptionInfo> getOptionInfo()
        {
            return optionInfo;
        }
    }

    private List<OptionInfo> getOptionInfo(List<Option> options)
    {
        List<OptionInfo> rootOptions = new ArrayList<OptionInfo>();

        if(null != options && !options.isEmpty())
        {
            for(Option option : options)
            {
                List<OptionInfo> childOptions = getOptionInfo(option.getChildOptions());
                OptionInfo optionInfo = new OptionInfo(option.getOptionId().toString(),option.getValue(),option.getSequence().toString(),option.getDisabled(),childOptions);
                rootOptions.add(optionInfo);
            }
        }

        return rootOptions;
    }
    
    private String getPluginKeyProvidingField(String fieldId)
    {
        String pluginKey = "";
        String fieldTypeClass = getCustomFieldTypeClassname(fieldId);
        Collection<ModuleDescriptor> moduleDescriptors = moduleDescriptorLocator.getEnabledModuleDescriptorsByModuleClassname(fieldTypeClass);
        if(null != moduleDescriptors && !moduleDescriptors.isEmpty())
        {
            pluginKey = moduleDescriptors.iterator().next().getPluginKey();
        }

        return pluginKey;
    }

    @Override
    public String getCustomFieldTypeClassname(String customFieldId)
    {
        String classname = null;

        CustomField cf = customFieldManager.getCustomFieldObject(customFieldId);
        if(null != cf)
        {
            CustomFieldType cfType = cf.getCustomFieldType();
            classname = cfType.getClass().getName();
        }

        return classname;
    }

    @Override
    public boolean isFromPlugin(String fieldId)
    {
        String fieldTypeClass = getCustomFieldTypeClassname(fieldId);

        return pluginHelper.isFromPlugin(fieldTypeClass);
    }

    @Override
    public String getPluginRelatedClassName(String fieldId)
    {
        String fieldTypeClass = getCustomFieldTypeClassname(fieldId);

        if (pluginHelper.isFromPlugin(fieldTypeClass))
        {
            return fieldTypeClass;
        }

        return null;
    }
}
