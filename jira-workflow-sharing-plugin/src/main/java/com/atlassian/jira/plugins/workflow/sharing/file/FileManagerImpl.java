package com.atlassian.jira.plugins.workflow.sharing.file;

import com.atlassian.jira.config.util.JiraHome;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.AgeFileFilter;
import org.apache.commons.lang.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;

@ExportAsService
@Component
public class FileManagerImpl implements FileManager, InitializingBean, DisposableBean
{
    private final Logger LOG = LoggerFactory.getLogger(FileManagerImpl.class);

    public static final String BASE_EXPORT_FOLDER = "workflowexports";

    private final JiraHome jiraHome;
    private final FileNameMapper exportFileNameMapper = new FileNameMapper();

    @Autowired
    public FileManagerImpl(JiraHome jiraHome)
    {
        this.jiraHome = jiraHome;
    }

    @Override
    public File createExportedWorkflowFile() throws IOException, CanNotCreateFileException
    {
        File file = null;

        File baseFolder = getExportDirectory();
        file = createFile(file, baseFolder);

        checkIsWritable(file);

        return file;
    }

    private File createFile(File file, File baseFolder) throws CanNotCreateFileException
    {
        int maxRetries = 100;

        for (int i = 0; i < maxRetries; i++)
        {
            String fileName = generateFileName();
            file = new File(baseFolder, fileName);

            if (!file.exists())
            {
                return file;
            }
        }

        throw new CanNotCreateFileException();
    }

    @Override
    public File getExportedWorkflowFile(String filename) throws IOException
    {
        File baseFolder = getExportDirectory();

        File exportFile = new File(baseFolder, filename);

        if (exportFile.getParentFile().getAbsolutePath().equals(baseFolder.getAbsolutePath())) {
            return exportFile;
        } else {
            throw new IllegalArgumentException(String.format("Requested file is not correct: %s", filename));
        }
    }

    private File getExportDirectory() throws IOException
    {
        File exportDir = new File(jiraHome.getExportDirectory(), BASE_EXPORT_FOLDER);

        FileUtils.forceMkdir(exportDir);

        return exportDir;
    }

    private void checkIsWritable(File file) throws CanNotCreateFileException
    {
        if (!file.exists())
        {
            try
            {
                if (!file.createNewFile())
                {
                    throw new CanNotCreateFileException();
                }
            }
            catch (IOException e)
            {
                throw new CanNotCreateFileException();
            }
        }

        if (!file.canWrite())
        {
            throw new CanNotCreateFileException();
        }

        FileUtils.deleteQuietly(file);
    }

    private String generateFileName()
    {
        return RandomStringUtils.randomAlphabetic(5);
    }

    @Override
    public void clearOlderThan(long ageInMs) throws IOException
    {
        LOG.debug("Deleting all workflow files older than " + ageInMs + " ms");

        clearOlderThan(getExportDirectory(), ageInMs, exportFileNameMapper);
    }

    @Override
    public void addExportedFileNameMapping(String actualName, String nameToDisplay)
    {
        exportFileNameMapper.addFile(actualName, nameToDisplay);
    }

    @Override
    public String getExportFileNameToDisplay(String actualFilename)
    {
        return exportFileNameMapper.getNameToDisplay(actualFilename);
    }

    @Override
    public void clearExportFileNameMapping(String actualName)
    {
        exportFileNameMapper.clearFile(actualName);
    }

    @Override
    public void delete(File file)
    {
        FileUtils.deleteQuietly(file);
    }

    private void clearOlderThan(File directory, long ageInMs, FileNameMapper fileNameMapper) throws IOException
    {
        File[] files = directory.listFiles((FileFilter) new AgeFileFilter(System.currentTimeMillis() - ageInMs, true));

        for (File file : files)
        {
            if (!FileUtils.deleteQuietly(file))
            {
                LOG.warn("File " + file + " could not be deleted");
            }
            else if (fileNameMapper != null)
            {
                fileNameMapper.clearFile(file.getName());
            }
        }
    }

    @Override
    public void afterPropertiesSet()
    {
        deleteAllWorkflowFiles();
    }

    @Override
    public void destroy()
    {
        deleteAllWorkflowFiles();
    }

    @Override
    public void deleteAllWorkflowFiles()
    {
        LOG.debug("Deleting all workflow files");

        exportFileNameMapper.clearAllFiles();

        try
        {
            FileUtils.cleanDirectory(getExportDirectory());
        }
        catch (Exception e)
        {
            LOG.debug("Could not clean the workflow export directory", e);
        }
    }
}
