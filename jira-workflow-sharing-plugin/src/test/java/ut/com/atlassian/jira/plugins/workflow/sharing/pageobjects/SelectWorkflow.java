package ut.com.atlassian.jira.plugins.workflow.sharing.pageobjects;

import com.atlassian.jira.webtest.webdriver.util.AUIFlags;
import com.atlassian.pageobjects.binder.Init;
import com.atlassian.pageobjects.binder.WaitUntil;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Conditions;
import com.atlassian.pageobjects.elements.query.Poller;
import com.google.common.base.Supplier;
import org.openqa.selenium.By;

import javax.annotation.PreDestroy;
import java.io.File;
import java.util.List;
import java.util.Optional;
import java.util.function.Function;
import java.util.function.Predicate;

public class SelectWorkflow extends WizardPage<SelectWorkflow, ChooseName>
{
    public static final String URL = "/plugins/servlet/wfshare-import";

    @ElementBy(id = "workflow-source-upm")
    private PageElement modeMarketplace;

    @ElementBy(id = "workflow-source-upload")
    private PageElement modeFromMyComputer;

    @ElementBy(id = "workflow-sharing-container")
    private PageElement workflowDataContainer;

    @ElementBy(id = "upload-container")
    private PageElement uploadContainer;

    @ElementBy(id = "wfShareImportFile")
    private PageElement fileUpload;

    AUIFlags auiFlags;

    @Init
    public void initialise() {
        auiFlags = pageBinder.bind(AUIFlags.class);
    }

    @Override
    public String getUrl()
    {
        return URL;
    }

    public boolean isModesNotPresent()
    {
        return !modeMarketplace.isPresent() && !modeFromMyComputer.isPresent();
    }

    public boolean isMarketplaceSelected()
    {
        return modeMarketplace.hasAttribute("aria-pressed","true");
    }

    public void selectMarketplace()
    {
        auiFlags.closeAllFlags();
        modeMarketplace.click();
    }

    public void selectMyComputer()
    {
        auiFlags.closeAllFlags();
        modeFromMyComputer.click();
    }

    public boolean isMarketplaceContainerVisible()
    {
        return isVisible(workflowDataContainer, uploadContainer);
    }

    public boolean isUploadContainerVisible()
    {
        return isVisible(uploadContainer, workflowDataContainer);
    }

    private boolean isVisible(PageElement visible, PageElement invisible)
    {
        return visible.isPresent() && visible.isVisible() && invisible.isPresent() && !invisible.isVisible();
    }

    public boolean isFileUploadVisible()
    {
        return fileUpload.isPresent() && fileUpload.isVisible();
    }

    public boolean isFileUploadInvisible()
    {
        return fileUpload.isPresent() && !fileUpload.isVisible();
    }

    public void setFile(File file)
    {
        fileUpload.type(file.getAbsolutePath());
    }

    @Override
    protected Class<ChooseName> getNextClass()
    {
        return ChooseName.class;
    }

    @Override
    protected Class<SelectWorkflow> getBackCass()
    {
        return SelectWorkflow.class;
    }

    public WorkflowSearchResultView findWorkflowInMarketplace(final String workflowSearchString) {
        selectMarketplace();

        if (!isMarketplaceContainerVisible()) {
            return null;
        }

        return pageBinder.bind(WorkflowSearchResultView.class, workflowSearchString);
    }

    public PageElement getWorkflowDataContainer() {
        return workflowDataContainer;
    }

    public static class WorkflowSearchResultView extends SelectWorkflow {

        private final String workflowSearchString;
        private PageElement previewAnchor;
        private PageElement lightBoxAnchor;

        public WorkflowSearchResultView(final String workflowName) {
            this.workflowSearchString = workflowName;
        }

        public WorkflowSearchResultView clickPreview() {
            this.previewAnchor.click();
            return this;
        }

        public WorkflowSearchResultView clickLightBox() {
            this.lightBoxAnchor.click();
            return this;
        }

        public boolean isPreviewAnchorVisible() {
            return previewAnchor.isVisible();
        }

        public boolean isLightBoxAnchorVisible() {
            return lightBoxAnchor.isVisible();
        }

        @WaitUntil
        public void doWait() {
            //Timeout for Pollers that wait for UPM responses to add elements. Timeout for UPM response is 60s so it is for Poller.
            final long UPM_RESPONSE_TIMEOUT = 60000L;

            PageElement seeMoreButton = getWorkflowDataContainer().find(By.className("workflow-sharing-see-more"));
            PageElement loadingContainer = getWorkflowDataContainer().find(By.className("workflow-sharing-loading-container"));

            boolean doLoop = true;
            while(doLoop) {
                Poller.waitUntilFalse(loadingContainer.timed().isVisible());
                if (seeMoreButton.isVisible()) {
                    seeMoreButton.click();
                } else {
                    doLoop = false;
                }
            }

            /*
            TODO change to lambdas after bump ASM major version to > 3;
            com.atlassian.plugins.atlassian-plugins-osgi-testrunner-bundle BundleTestScanner uses ASM 3.1 which doesn't recognize lambdas
            and causes java.lang.ArrayIndexOutOfBoundsException being thrown by org.objectweb.asm.ClassReader
            */
            final Predicate<PageElement> workflowNamePredicate = new Predicate<PageElement>() {
                @Override
                public boolean test(PageElement pageElement) {
                    return workflowSearchString.equals(pageElement.getText());
                }
            };

            final Function<PageElement, Optional<PageElement>> workflowSearchFn = new Function<PageElement, Optional<PageElement>>() {
                @Override
                public Optional<PageElement> apply(PageElement container) {
                    return container.findAll(By.className("workflow-name")).stream().filter(workflowNamePredicate).findFirst();
                }
            };

            Poller.waitUntilTrue(Conditions.forSupplier(UPM_RESPONSE_TIMEOUT, new Supplier<Boolean>() {
                @Override
                public Boolean get() {
                    return workflowSearchFn.apply(getWorkflowDataContainer()).isPresent();
                }
            }));

            final List<PageElement> workflowEntries = getWorkflowDataContainer().findAll(By.className("workflow-entry"));

            for(PageElement entry : workflowEntries) {
                PageElement workflowNameElement = entry.find(By.className("workflow-name"));
                if (workflowNamePredicate.test(workflowNameElement)) {
                    this.previewAnchor = entry.find(By.className("workflow-image"));
                    this.lightBoxAnchor = entry.find(By.className("workflow-image-lightbox"));

                    final Supplier<Boolean> isPreviewPresent = new Supplier<Boolean>() {
                        @Override
                        public Boolean get() {
                            return previewAnchor.isPresent();
                        }
                    };

                    final Supplier<Boolean> isLightBoxPresent = new Supplier<Boolean>() {
                        @Override
                        public Boolean get() {
                            return lightBoxAnchor.isPresent();
                        }
                    };

                    Poller.waitUntilTrue(Conditions.forSupplier(UPM_RESPONSE_TIMEOUT, isPreviewPresent));
                    Poller.waitUntilTrue(Conditions.forSupplier(UPM_RESPONSE_TIMEOUT, isLightBoxPresent));
                    break;
                }
            }
        }
    }
}
