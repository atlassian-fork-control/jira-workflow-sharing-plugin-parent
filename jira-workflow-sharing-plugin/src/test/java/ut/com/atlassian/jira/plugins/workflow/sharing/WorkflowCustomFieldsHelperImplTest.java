package ut.com.atlassian.jira.plugins.workflow.sharing;

import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.customfields.option.Option;
import com.atlassian.jira.issue.customfields.option.Options;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.config.FieldConfig;
import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.ofbiz.OfBizDelegator;
import com.atlassian.jira.plugins.workflow.sharing.ModuleDescriptorLocator;
import com.atlassian.jira.plugins.workflow.sharing.WorkflowCustomFieldsHelperImpl;
import com.atlassian.jira.plugins.workflow.sharing.WorkflowPluginHelper;
import com.google.common.collect.Lists;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.AbstractList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class WorkflowCustomFieldsHelperImplTest
{
    @Mock
    private CustomFieldManager customFieldManager;

    @Mock
    private OfBizDelegator ofBizDelegator;

    @Mock
    private ModuleDescriptorLocator moduleDescriptorLocator;

    @Mock
    private WorkflowPluginHelper pluginHelper;

    @Mock
    private CustomField customField;

    private WorkflowCustomFieldsHelperImpl workflowCustomFieldsHelper;


    @Before
    public void setUp()
    {
        workflowCustomFieldsHelper = new WorkflowCustomFieldsHelperImpl(customFieldManager,
                moduleDescriptorLocator, pluginHelper);
    }

    @Test
    public void optionsAreEmptyIfNoConfigurationScheme()
    {
        when(customField.getConfigurationSchemes()).thenReturn(Lists.<FieldConfigScheme>newArrayList());

        WorkflowCustomFieldsHelperImpl.ConfigurationSchemeInfo schemeInfo = workflowCustomFieldsHelper.getConfigurationSchemeInfo(customField);

        assertTrue(schemeInfo.getOptionInfo().isEmpty());
    }

    @Test
    public void optionsAreEmptyIfConfigurationSchemeHasNoOptions()
    {
        FieldConfigScheme scheme = mock(FieldConfigScheme.class);
        FieldConfig fieldConfig = mock(FieldConfig.class);

        when(scheme.getOneAndOnlyConfig()).thenReturn(fieldConfig);
        when(customField.getConfigurationSchemes()).thenReturn(Arrays.asList(scheme));
        when(customField.getOptions(null, fieldConfig, null)).thenReturn(null);
        when(fieldConfig.getId()).thenReturn(1L);

        WorkflowCustomFieldsHelperImpl.ConfigurationSchemeInfo schemeInfo = workflowCustomFieldsHelper.getConfigurationSchemeInfo(customField);

        assertTrue(schemeInfo.getOptionInfo().isEmpty());
    }

    @Test
    public void optionsAreRetrievedIfTheyExist()
    {
        FieldConfigScheme scheme = mock(FieldConfigScheme.class);
        FieldConfig fieldConfig = mock(FieldConfig.class);

        Option option = mock(Option.class);
        Options options = new MockOptions(option);

        when(scheme.isGlobal()).thenReturn(true);
        when(scheme.getOneAndOnlyConfig()).thenReturn(fieldConfig);
        when(customField.getConfigurationSchemes()).thenReturn(Arrays.asList(scheme));
        when(customField.getOptions(null, fieldConfig, null)).thenReturn(options);
        when(option.getValue()).thenReturn("My value");
        when(option.getOptionId()).thenReturn(1L);
        when(fieldConfig.getId()).thenReturn(1L);

        WorkflowCustomFieldsHelperImpl.ConfigurationSchemeInfo schemeInfo = workflowCustomFieldsHelper.getConfigurationSchemeInfo(customField);

        assertEquals(1, schemeInfo.getOptionInfo().size());
        assertEquals("My value", schemeInfo.getOptionInfo().get(0).getValue());
        assertEquals("1", schemeInfo.getOptionInfo().get(0).getOriginalId());
    }

    @Test
    public void globalOptionsAreRetrievedIfMultipleSchemes()
    {
        FieldConfigScheme nonGlobalScheme = mock(FieldConfigScheme.class);
        FieldConfigScheme globalScheme = mock(FieldConfigScheme.class);
        FieldConfig fieldConfig = mock(FieldConfig.class);

        Option option = mock(Option.class);
        Options options = new MockOptions(option);

        when(nonGlobalScheme.isGlobal()).thenReturn(false);
        when(globalScheme.isGlobal()).thenReturn(true);
        when(globalScheme.getOneAndOnlyConfig()).thenReturn(fieldConfig);
        when(customField.getConfigurationSchemes()).thenReturn(Arrays.asList(globalScheme, nonGlobalScheme));
        when(customField.getOptions(null, fieldConfig, null)).thenReturn(options);
        when(option.getValue()).thenReturn("My value");
        when(option.getOptionId()).thenReturn(1L);
        when(fieldConfig.getId()).thenReturn(1L);

        WorkflowCustomFieldsHelperImpl.ConfigurationSchemeInfo schemeInfo = workflowCustomFieldsHelper.getConfigurationSchemeInfo(customField);

        assertEquals(1, schemeInfo.getOptionInfo().size());
        assertEquals("My value", schemeInfo.getOptionInfo().get(0).getValue());
        assertEquals("1", schemeInfo.getOptionInfo().get(0).getOriginalId());
    }

    private static class MockOptions extends AbstractList<Option> implements Options
    {
        private List<Option> delegate = Lists.newArrayList();

        private MockOptions(Option...options)
        {
            this.delegate.addAll(Arrays.asList(options));
        }

        @Override
        public List<Option> getRootOptions()
        {
            return delegate;
        }

        @Override
        public Option getOptionById(Long aLong)
        {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public Option getOptionForValue(String s, Long aLong)
        {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public Option addOption(Option option, String s)
        {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public void removeOption(Option option)
        {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public void moveToStartSequence(Option option)
        {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public void incrementSequence(Option option)
        {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public void decrementSequence(Option option)
        {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public void moveToLastSequence(Option option)
        {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public void setValue(Option option, String s)
        {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public void enableOption(Option option)
        {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public void disableOption(Option option)
        {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public FieldConfig getRelatedFieldConfig()
        {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public void sortOptionsByValue(Option option)
        {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public void moveOptionToPosition(Map<Integer, Option> integerOptionMap)
        {
            throw new UnsupportedOperationException("Not implemented");
        }

        @Override
        public int size()
        {
            return delegate.size();
        }

        @Override
        public Option get(final int index)
        {
            return delegate.get(index);
        }
    }
}
