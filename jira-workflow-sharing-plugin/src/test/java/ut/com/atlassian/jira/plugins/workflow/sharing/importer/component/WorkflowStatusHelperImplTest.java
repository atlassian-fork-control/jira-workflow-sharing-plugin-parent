package ut.com.atlassian.jira.plugins.workflow.sharing.importer.component;

import com.atlassian.jira.bc.config.StatusService;
import com.atlassian.jira.config.ConstantsManager;
import com.atlassian.jira.issue.status.Status;
import com.atlassian.jira.plugins.workflow.sharing.importer.ValidNameGenerator;
import com.atlassian.jira.plugins.workflow.sharing.importer.component.WorkflowStatusHelperImpl;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.sal.api.message.I18nResolver;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;

import static org.apache.commons.lang.StringUtils.abbreviate;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class WorkflowStatusHelperImplTest
{
    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;

    @Mock
    private StatusService statusService;

    @Mock
    private I18nHelper i18nHelper;

    @Mock
    private ConstantsManager constantsManager;

    @Mock
    private I18nResolver i18n;

    @Mock
    private Status statusOne;

    @Mock
    private Status statusTwo;

    private WorkflowStatusHelperImpl workflowStatusHelper;

    @Before
    public void setUp()
    {
        ValidNameGenerator validNameGenerator = new ValidNameGenerator(jiraAuthenticationContext);

        workflowStatusHelper = new WorkflowStatusHelperImpl(constantsManager, i18n, validNameGenerator, jiraAuthenticationContext, statusService);

        when(jiraAuthenticationContext.getI18nHelper()).thenReturn(i18nHelper);
    }

    @Test
    public void nameStaysTheSameIfThereIsNoExisting()
    {
        when(constantsManager.getStatuses()).thenReturn(Arrays.asList(statusOne));
        when(statusOne.getName()).thenReturn("Something else");

        String name = workflowStatusHelper.getValidStatusName("No duplicate");

        assertEquals("No duplicate", name);
    }

    @Test
    public void abbreviatesIfLengthIsExceeded()
    {
        when(constantsManager.getStatuses()).thenReturn(Arrays.asList(statusOne));
        when(statusOne.getName()).thenReturn("Something else");

        String name = workflowStatusHelper.getValidStatusName(StringUtils.repeat("a", 61));

        assertEquals(StringUtils.repeat("a", 57) + "...", name);
    }

    @Test
    public void addsPrefixOfTwoIfThereIsExisting()
    {
        when(constantsManager.getStatuses()).thenReturn(Arrays.asList(statusOne));
        when(statusOne.getName()).thenReturn("Existing");
        when(i18nHelper.getText("wfshare.import.copy.prefix", "2")).thenReturn(" - 2");
        when(i18nHelper.getText("wfshare.import.copy", "Existing", " - 2")).thenReturn("Existing - 2");

        String name = workflowStatusHelper.getValidStatusName("Existing");

        assertEquals("Existing - 2", name);
    }

    @Test
    public void addsPrefixOfThreeOfIfThereIsExistingAndWithPrefixOfTwo()
    {
        when(constantsManager.getStatuses()).thenReturn(Arrays.asList(statusOne, statusTwo));
        when(statusOne.getName()).thenReturn("Existing");
        when(statusTwo.getName()).thenReturn("Existing - 2");
        when(i18nHelper.getText("wfshare.import.copy.prefix", "2")).thenReturn(" - 2");
        when(i18nHelper.getText("wfshare.import.copy.prefix", "3")).thenReturn(" - 3");
        when(i18nHelper.getText("wfshare.import.copy", "Existing", " - 2")).thenReturn("Existing - 2");
        when(i18nHelper.getText("wfshare.import.copy", "Existing", " - 3")).thenReturn("Existing - 3");

        String name = workflowStatusHelper.getValidStatusName("Existing");

        assertEquals("Existing - 3", name);
    }

    @Test
    public void addsPrefixOfTwoAndAbbreviatesIfThereIsExistingAndLengthIsExceeded()
    {
        String existingName = StringUtils.repeat("a", 60);

        when(constantsManager.getStatuses()).thenReturn(Arrays.asList(statusOne));
        when(statusOne.getName()).thenReturn(existingName);
        when(i18nHelper.getText("wfshare.import.copy.prefix", "2")).thenReturn(" - 2");

        String abbreviatedName = abbreviate(existingName, 56);
        when(i18nHelper.getText("wfshare.import.copy", abbreviatedName, " - 2")).thenReturn(abbreviatedName + " - 2");

        String name = workflowStatusHelper.getValidStatusName(existingName);

        assertEquals(StringUtils.repeat("a", 53) + "... - 2", name);
    }

    @Test
    public void addsPrefixOfThreeAndAbbreviatesIfThereIsExistingAndWithPrefixOfTwoAndLengthIsExceeded()
    {
        String existingName = StringUtils.repeat("a", 60);
        String existingWithPrefixOfTwo= StringUtils.repeat("a", 53) + "... - 2";

        when(constantsManager.getStatuses()).thenReturn(Arrays.asList(statusOne, statusTwo));
        when(statusOne.getName()).thenReturn(existingName);
        when(statusTwo.getName()).thenReturn(existingWithPrefixOfTwo);
        when(i18nHelper.getText("wfshare.import.copy.prefix", "2")).thenReturn(" - 2");
        when(i18nHelper.getText("wfshare.import.copy.prefix", "3")).thenReturn(" - 3");

        String abbreviatedName = abbreviate(existingName, 56);
        when(i18nHelper.getText("wfshare.import.copy", abbreviatedName, " - 2")).thenReturn(abbreviatedName + " - 2");
        when(i18nHelper.getText("wfshare.import.copy", abbreviatedName, " - 3")).thenReturn(abbreviatedName + " - 3");

        String name = workflowStatusHelper.getValidStatusName(existingName);

        assertEquals(StringUtils.repeat("a", 53) + "... - 3", name);
    }
}
