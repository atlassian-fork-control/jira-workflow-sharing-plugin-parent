package it.com.atlassian.jira.plugins.workflow.sharing.wired;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.atlassian.jira.exception.RemoveException;
import com.atlassian.jira.issue.CustomFieldManager;
import com.atlassian.jira.issue.context.GlobalIssueContext;
import com.atlassian.jira.issue.customfields.CustomFieldType;
import com.atlassian.jira.issue.fields.CustomField;
import com.atlassian.jira.issue.fields.screen.FieldScreen;
import com.atlassian.jira.issue.fields.screen.FieldScreenImpl;
import com.atlassian.jira.issue.fields.screen.FieldScreenManager;
import com.atlassian.jira.plugins.workflow.sharing.WorkflowScreensHelper;
import com.atlassian.jira.workflow.ConfigurableJiraWorkflow;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;

import com.google.common.collect.Iterables;
import com.opensymphony.workflow.loader.ActionDescriptor;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.ofbiz.core.entity.GenericEntityException;

import ut.com.atlassian.jira.plugins.workflow.sharing.utils.TestConstants;
import ut.com.atlassian.jira.plugins.workflow.sharing.utils.WorkflowTestUtils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(AtlassianPluginsTestRunner.class)
public class WorkflowScreensHelperTest
{
    private final WorkflowScreensHelper workflowScreensHelper;
    private final CustomFieldManager customFieldManager;
    private final WorkflowManager workflowManager;
    private final FieldScreenManager fieldScreenManager;
    private final WorkflowTestUtils workflowTestUtils;


    private CustomField myTextField;
    private FieldScreen screenWithCustomField;
    private FieldScreen copyOfScreenWithCustomField;

    public WorkflowScreensHelperTest(WorkflowScreensHelper workflowScreensHelper, CustomFieldManager customFieldManager, WorkflowManager workflowManager, FieldScreenManager fieldScreenManager, WorkflowTestUtils workflowTestUtils)
    {
        this.workflowScreensHelper = workflowScreensHelper;
        this.customFieldManager = customFieldManager;
        this.workflowManager = workflowManager;
        this.fieldScreenManager = fieldScreenManager;
        this.workflowTestUtils = workflowTestUtils;
    }

    @BeforeClass
    public void setupData() throws GenericEntityException
    {
        CustomFieldType cfType = customFieldManager.getCustomFieldType(TestConstants.TEXT_FIELD_CF_KEY);

        List issueTypes = new ArrayList(1);
        issueTypes.add(null);

        this.myTextField = customFieldManager.createCustomField("myTextField", "", cfType, null, Arrays.asList(GlobalIssueContext.getInstance()), issueTypes);
        screenWithCustomField = new FieldScreenImpl(fieldScreenManager);
        screenWithCustomField.setName("screenWithCustomField");
        screenWithCustomField.addTab("sometab");
        screenWithCustomField.getTab(0).addFieldScreenLayoutItem(myTextField.getId());

        copyOfScreenWithCustomField = new FieldScreenImpl(fieldScreenManager);
        copyOfScreenWithCustomField.setName("screenWithCustomField");
        copyOfScreenWithCustomField.addTab("sometab");
        copyOfScreenWithCustomField.getTab(0).addFieldScreenLayoutItem(myTextField.getId());
    }

    @AfterClass
    public void removeData() throws RemoveException
    {
        fieldScreenManager.removeFieldScreen(screenWithCustomField.getId());
        fieldScreenManager.removeFieldScreen(copyOfScreenWithCustomField.getId());
        customFieldManager.removeCustomField(myTextField);
        workflowTestUtils.clean();
    }

    @Test
    public void classnamesContainsOurField() throws Exception
    {
        Set<String> classnames = workflowScreensHelper.getClassnamesForScreen(screenWithCustomField.getId().toString());
        assertTrue("expected classnames to contain " + TestConstants.TEXT_FIELD_CF_CLASS + " but it didn't", classnames.contains(TestConstants.TEXT_FIELD_CF_CLASS));
    }

    @Test
    public void classnamesEmptyForBadId() throws Exception
    {
        Set<String> classnames = workflowScreensHelper.getClassnamesForScreen("somecrapId");
        assertTrue("expected classnames to be empty but it was not", classnames.isEmpty());
    }

    @Test
    public void actionClassnamesContainsOurField() throws Exception
    {
        JiraWorkflow jw = workflowTestUtils.createWorkflowWithCustomScreen("customScreenWorkflow", screenWithCustomField);

        try
        {
            ActionDescriptor action = jw.getActionsForScreen(screenWithCustomField).iterator().next();

            Set<String> classnames = workflowScreensHelper.getClassnamesForScreenFromAction(action);
            assertTrue("expected classnames to contain " + TestConstants.TEXT_FIELD_CF_CLASS + " but it didn't", classnames.contains(TestConstants.TEXT_FIELD_CF_CLASS));
        }
        finally
        {
            workflowManager.deleteWorkflow(jw);
        }
    }

    @Test
    public void actionClassnamesEmptyForBadScreen() throws Exception
    {
        JiraWorkflow jw = workflowTestUtils.createNoopWorkflow("noopFunctionWorkflow", myTextField);

        try
        {
            ActionDescriptor action = jw.getAllActions().iterator().next();

            Set<String> classnames = workflowScreensHelper.getClassnamesForScreenFromAction(action);
            assertTrue("expected classnames to be empty but it was not", classnames.isEmpty());
        }
        finally
        {
            workflowManager.deleteWorkflow(jw);
        }
    }

    @Test
    public void idsContainsOurFieldForScreen() throws Exception
    {
        JiraWorkflow jw = workflowTestUtils.createWorkflowWithCustomScreen("customScreenWorkflow", screenWithCustomField);

        try
        {

            Iterable<String> ids = workflowScreensHelper.getCustomFieldIdsForWorkflowScreens(jw);
            assertTrue("expected ids to contain " + myTextField.getId() + " but it didn't", Iterables.contains(ids, myTextField.getId()));
        }
        finally
        {
            workflowManager.deleteWorkflow(jw);
        }
    }

    @Test
    public void idsEmptyForNoField() throws Exception
    {
        JiraWorkflow jw = workflowManager.getDefaultWorkflow();
        Iterable<String> ids = workflowScreensHelper.getCustomFieldIdsForWorkflowScreens(jw);
        assertTrue("expected ids to be empty but it was not", Iterables.isEmpty(ids));
    }

    @Test
    public void screenIdGetsUpdated() throws Exception
    {
        ConfigurableJiraWorkflow jw = workflowTestUtils.createWorkflowWithCustomScreen("customScreenWorkflow", screenWithCustomField);

        try
        {
            Map<Long, Long> idMapping = new HashMap<Long, Long>();
            idMapping.put(screenWithCustomField.getId(), copyOfScreenWithCustomField.getId());

            workflowScreensHelper.updateWorkflowScreenIds(jw, idMapping);

            JiraWorkflow updatedWorkflow = workflowManager.getWorkflow("customScreenWorkflow");

            int foundActionCount = 0;

            Collection<ActionDescriptor> actions = updatedWorkflow.getAllActions();

            for (ActionDescriptor action : actions)
            {
                if (WorkflowScreensHelper.FIELDSCREEN_VIEW.equals(action.getView()))
                {
                    foundActionCount++;
                    Map<String, String> meta = action.getMetaAttributes();

                    assertEquals("wrong field id in updated workflow", copyOfScreenWithCustomField.getId().toString(), meta.get("jira.fieldscreen.id"));
                }
            }

            assertTrue("should have processed at least 1 action", (foundActionCount > 0));
        }
        finally
        {
            workflowManager.deleteWorkflow(jw);
        }
    }

    @Test
    public void screenIdGetsUpdatedToNewValue() throws Exception
    {
        ConfigurableJiraWorkflow jw = workflowTestUtils.createWorkflowWithCustomScreen("customScreenWorkflow", screenWithCustomField);

        try
        {
            Map<Long, Long> idMapping = new HashMap<Long, Long>();
            idMapping.put(screenWithCustomField.getId(), 5000L);

            workflowScreensHelper.updateWorkflowScreenIds(jw, idMapping);

            JiraWorkflow updatedWorkflow = workflowManager.getWorkflow("customScreenWorkflow");

            int foundActionCount = 0;

            Collection<ActionDescriptor> actions = updatedWorkflow.getAllActions();

            for (ActionDescriptor action : actions)
            {
                if (WorkflowScreensHelper.FIELDSCREEN_VIEW.equals(action.getView()))
                {
                    foundActionCount++;
                    Map<String, String> meta = action.getMetaAttributes();

                    assertEquals("wrong field id in updated workflow", "5000", meta.get("jira.fieldscreen.id"));
                }
            }

            assertTrue("should have processed at least 1 action", (foundActionCount > 0));
        }
        finally
        {
            workflowManager.deleteWorkflow(jw);
        }
    }

}
